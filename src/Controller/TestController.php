<?php

namespace App\Controller;

use App\Entity\Question;
use App\Entity\Test;
use App\Form\Type\QuestionType;
use App\Form\Type\TestType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TestController
 * @package App\Controller
 *
 * @Route("/admin")
 */
class TestController extends AbstractController
{
    /**
     * Creating a new Test entity.
     *
     * @Route("/test/create", name="test_create")
     */
    public function create(Request $request)
    {
        $test = new Test();

        $form = $this->createForm(TestType::class, $test)
            ->add('save', SubmitType::class, array('label' => 'Create'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $test = $form->getData();
            try {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($test);
                $entityManager->flush();
                $this->addFlash('success', 'Success creating');
                return $this->redirectToRoute('test_update', ['id' => $test->getId()]);
            } catch (\Exception $e) {
                $this->addFlash('danger', 'Error in creating entity "Test"');
            }
        }

        return $this->render('test/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Updating a Test entity.
     *
     * @Route("/test/update/{id<\d+>}", name="test_update")
     */
    public function update(Request $request, $id)
    {
        $test = $this->getDoctrine()->getRepository(Test::class)->find($id);

        if (!$test) {
            throw $this->createNotFoundException('404 page not found');
        }

        $form = $this->createForm(TestType::class, $test)
            ->add('save', SubmitType::class, array('label' => 'Update'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $test = $form->getData();
            try {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->flush();

                $this->addFlash('success', 'Success updating');
                return $this->redirectToRoute('test_update', ['id' => $test->getId()]);
            } catch (\Exception $e) {
                $this->addFlash('danger', 'Error in updating entity "Test"');
            }
        }

        return $this->render('test/update.html.twig', [
            'form' => $form->createView(),
            'test' => $test
        ]);
    }

    /**
     * Deleting a Test entity.
     *
     * @Route("/test/delete/{id<\d+>}", name="test_delete")
     */
    public function delete(Test $test)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($test);
        $em->flush();

        $this->addFlash('success', 'Success deleting');

        return $this->redirectToRoute('site');
    }

    /**
     * Creating a new Question entity and new Answers to this question.
     *
     * @Route("/test/create-question/{id<\d+>}", name="test_create_question")
     */
    public function createQuestion(Request $request, Test $test)
    {
        $question = new Question();
        $question->setTest($test);

        $form = $this->createForm(QuestionType::class, $question)
            ->add('save', SubmitType::class, array('label' => 'Create'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $question = $form->getData();
            $answers = $form->getData()->getAnswers();
            try {
                $entityManager = $this->getDoctrine()->getManager();
                foreach ($answers as $answer) {
                    $answer->setQuestion($question);
                    $entityManager->persist($answer);
                }
                $entityManager->persist($question);
                $entityManager->flush();
                $this->addFlash('success', 'Success creating');
                return $this->redirectToRoute('test_update', ['id' => $test->getId()]);
            } catch(\Exception $e) {
                $this->addFlash('danger', 'Error in creating entity "Question"');
            }
        }

        return $this->render('test/create-question.html.twig', [
            'form' => $form->createView(),
            'test' => $test
        ]);
    }

    /**
     * Updating a Question entity and creating, editing an answers to this question.
     * @Route("/test/update-question/{id<\d+>}", name="test_update_question")
     */
    public function updateQuestion(Request $request, Question $question)
    {

        $form = $this->createForm(QuestionType::class, $question)
            ->add('save', SubmitType::class, array('label' => 'Create'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $question = $form->getData();
            $answers = $form->getData()->getAnswers();
            try {
                $entityManager = $this->getDoctrine()->getManager();
                foreach ($answers as $answer) {
                    $answer->setQuestion($question);
                    $entityManager->persist($answer);
                }
                $entityManager->flush();
                $this->addFlash('success', 'Success updating');
                return $this->redirectToRoute('test_update', ['id' => $question->getTest()->getId()]);
            } catch(\Exception $e) {
                $this->addFlash('danger', 'Error in updating entity "Question"');
            }
        }

        return $this->render('test/update-question.html.twig', [
            'form' => $form->createView(),
            'question' => $question
        ]);
    }

    /**
     * Deleting a Question entity.
     *
     * @Route("/test/delete-question/{id<\d+>}", name="test_delete_question")
     */
    public function deleteQuestion(Question $question)
    {
        $id = $question->getTest()->getId();
        $em = $this->getDoctrine()->getManager();
        $em->remove($question);
        $em->flush();

        $this->addFlash('success', 'Success deleting');

        return $this->redirectToRoute('test_update', ['id' => $id]);
    }

}
