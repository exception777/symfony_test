<?php

namespace App\Controller;

use App\Entity\Test;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SiteController
 * @package App\Controller
 */
class SiteController extends AbstractController
{
    /**
     * Lists all Test entities.
     *
     * @Route("/", name="site")
     */
    public function index()
    {
        $tests = $this->getDoctrine()->getRepository(Test::class)->findBy([], ['id' => 'DESC']);

        return $this->render('site/index.html.twig', [
            'tests' => $tests,
        ]);
    }

    /**
     * Displays a test.
     *
     * @Route("/test/{id<\d+>}", name="test")
     */
    public function test(Request $request, SessionInterface $session, Test $test)
    {
        // Creating a form with test questions
        $form = $this->createFormBuilder();
        foreach ($test->getQuestions() as $question) {
            $choices = [];
            foreach ($question->getAnswers() as $answer) {
                $choices[$answer->getName()] = $answer->getId();
            }
            $form->add($question->getId(), ChoiceType::class, [
                'choices' => $choices,
                'multiple' => true,
                'expanded' => true,
                'label' => $question->getName()
            ]);
        }
        $form->add('submit', SubmitType::class, ['label' => 'Submit', 'attr' => ['class' => 'btn btn-primary']]);
        $form = $form->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            /**
             * Checking the correct answers to questions.
             * If all the answers to the question coincide, add 1 point to the variable $point.
             */
            $point = 0;
            foreach ($data as $questionId => $answers) {
                foreach ($test->getQuestions() as $question) {
                    if ($question->getId() != $questionId) {
                        continue;
                    }
                    $rightAnswersId = [];
                    foreach ($question->getAnswers() as $answer) {
                        if ($answer->getIsRight()) {
                            $rightAnswersId[] = $answer->getId();
                        }
                    }
                    if (count($answers) == count($rightAnswersId) && empty(array_diff($rightAnswersId, $answers))) {
                        $point++;
                    }
                    break;
                }
            }
            $session->set('point', $point);

            return $this->redirectToRoute('test_success', ['id' => $test->getId()]);
        }

        return $this->render('site/test.html.twig', [
            'test' => $test,
            'form' => $form->createView()
        ]);
    }

    /**
     * Displays result test.
     *
     * @Route("/test/success/{id<\d+>}", name="test_success")
     */
    public function testSuccess(SessionInterface $session, Test $test)
    {
        return $this->render('site/test-success.html.twig', [
            'test' => $test,
            'point' => $session->get('point')
        ]);
    }
}
